public abstract class SpaceTech {
    private String creatorName;
    private int yearOfCreation;

    public  SpaceTech(String creatorName, int yearOfCreation){
        this.creatorName = creatorName;
        this.yearOfCreation = yearOfCreation;
    }
    public abstract   void destruction();

    public String getCreatorName() {
        return creatorName;
    }

    public int getYearOfCreation() {
        return yearOfCreation;
    }
}
