public class Debris implements Floatable {
    private String id;
    private  String material;
    private  String parentName;
    private  double dist;
    public Debris(String id, String material, String parentName, double dist){
        this.id = id;
        this.material = material;
        this.parentName = parentName;
        this.dist = dist;
    }
    public void floatAroundParent(){
        System.out.println("I fly around planet "+parentName );
    }
    public  void changeDistance(double distanceFromParent){
        System.out.println("The change is "+ dist);
    }
}
