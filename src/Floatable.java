public interface Floatable {
     void floatAroundParent();
     void changeDistance(double distanceFromParent);
}
