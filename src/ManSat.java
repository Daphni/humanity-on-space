public class ManSat extends SpaceTech implements Floatable {
    private  String nameOfSat;
    private  String purpose;
    private int crew;
    private  String parentName;
    private  double dist;

    public ManSat(String creatorName,int yearOfCreation,String nameOfSat, String purpose, int crew,
                  String parentName, double dist1){
       super(creatorName, yearOfCreation);
        this.nameOfSat = nameOfSat;
        this.purpose = purpose;
        this.crew = crew;
        this.parentName = parentName;
        this.dist = dist;
    }
    public void floatAroundParent(){
        System.out.println("I fly around planet "+parentName );
    }
    public  void changeDistance(double distanceFromParent){
        System.out.println("The change is "+dist );
    }
    public   void destruction(){
            System.out.println("Self destruction haw began");
    }

    public String getNameOfSat() {
        return nameOfSat;
    }

    public String getPurpose() {
        return purpose;
    }

    public int getCrew() {
        return crew;
    }

    public String getParentName() {
        return parentName;
    }

    public double getDist() {
        return dist;
    }
}
