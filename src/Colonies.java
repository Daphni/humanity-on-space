public class Colonies extends Satellite {
    private String nameOfColony;
    private String nameOfMayor;
    private long population;
    private boolean warOutcome;
    private boolean peaceOutcome;
    public  Colonies(String planetName, double diameter, double perOfWater, int increNumFromSun,String parentPlanet,
                     double dist,String nameOfColony, String nameOfMayor,long population){
        super ( planetName, diameter, perOfWater, increNumFromSun, parentPlanet , dist);
        this.nameOfColony = nameOfColony;
        this.nameOfMayor = nameOfMayor;
        this.population = population;
    }
    public void war( String nameOfColony,boolean warOutcome){
        System.out.println("There s been a war with Colony "+nameOfColony+" the outcome was "+warOutcome);
    }
    public void peace (String nameOfColony, boolean peaceOutcome){
        System.out.println("There s been declare peace with Colony "+nameOfColony+" the outcome was "+peaceOutcome);
    }
    public  void  explore(){
        System.out.println("We explore the universe");
    }
    public ManSat launchSatellite(String creatorName,int yearOfCreation,String nameOfSat, String purpose, int crew,
                                  String parentName, double dist1){
        return new ManSat(creatorName, yearOfCreation,nameOfSat, purpose, crew,
        parentName, dist1);
    }
}
