public class DefensiveWeapons extends SpaceTech implements Floatable {
    private String id;
    private String flag;
    private int rockets;
    private  String parentName;
    private  double dist;
    public DefensiveWeapons(String creatorName,int yearOfCreation,String id, String flag, int rockets,
                            String parentName,double dist){
        super(creatorName,yearOfCreation);
        this.id = id;
        this.flag = flag;
        this.rockets = rockets;
        this.parentName = parentName;
        this.dist= dist;
    }
    public DefensiveWeapons(String creatorName,int yearOfCreation,String id, int rockets,String parentName,double dist) {
        super(creatorName,yearOfCreation);
        this.id = id;
        this.rockets = rockets;
        this.parentName = parentName;
        this.dist = dist;
    }
    public void floatAroundParent(){
        System.out.println("I fly around planet "+parentName );
    }
    public  void changeDistance(double distanceFromParent){
        System.out.println("The change is "+dist );
    }
    public  void destruction(){
        System.out.println("Self destruction haw began");
    }
}
