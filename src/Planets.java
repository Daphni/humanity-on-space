public abstract class Planets {
    private String planetName;
    private double diameter;
    private  double perOfWater;
    private int increNumFromSun;
    private double longitude;
    private double latitude;
    public Planets(String planetName, double diameter, double perOfWater, int increNumFromSun){
        this.planetName= planetName;
        this.diameter = diameter;
        this.perOfWater = perOfWater;
        this.increNumFromSun = increNumFromSun;
    }

    public void rotAroundSun(){
        System.out.println("The planet "+planetName+"is rotating around the sun");
    }
    public void rotAroundSelf(){
        System.out.println("The planet "+planetName+"is rotating around itself");
    }
    public void  earhtquake(double longitude,double latitude ){
        System.out.println("There s been an earthquake at longitude =" + longitude + " and latitude="+latitude);
    }

    public String getPlanetName() {
        return planetName;
    }

    public int getIncreNumFromSun() {
        return increNumFromSun;
    }
}
