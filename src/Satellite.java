public class Satellite extends Planets implements  Floatable{
   private  String parentPlanet;
    private   double dist;
    public  Satellite(String planetName, double diameter, double perOfWater, int increNumFromSun,String parentPlanet,
                       double dist) {
        super(planetName, diameter, perOfWater, increNumFromSun);
        this.parentPlanet = parentPlanet;
        this.dist = dist;
    }
    public void floatAroundParent(){
        System.out.println("I fly around planet "+parentPlanet );
    }
    public  void changeDistance(double distanceFromParent){
        System.out.println("The change is "+ dist );
    }
}
